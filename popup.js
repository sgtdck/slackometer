// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;
}

function populateDomainsTextarea() {
    chrome.storage.local.get('domains', function(items) {
        if(typeof items.domains !== 'undefined') {
            document.getElementById("domains").value = items.domains.join("\n");
        }
    });
}

function showReport() {
    var backgroundPage = chrome.extension.getBackgroundPage();
    var key = 'log.days.' + backgroundPage.getTodayString();

    var tbodyRef = document.getElementById('report-table').getElementsByTagName('tbody')[0];

    backgroundPage.getDailyReport(function(items) {
        var sortable = [];
        for(var domain in items[key]) {
            sortable.push({"domain": domain, "millis": items[key][domain]});
        }
        sortable.sort(function(a, b) {
           return b.millis - a.millis;
        });
        var total = 0;

        for(var i = 0; i < sortable.length; ++i) {
            var newRow = tbodyRef.insertRow(tbodyRef.rows.length);
            newRow.insertCell(0).appendChild(document.createTextNode(sortable[i].domain));
            newRow.insertCell(1).appendChild(document.createTextNode(backgroundPage.getTimeString(sortable[i].millis, true)));

            total += sortable[i].millis;
        }

        var tfootRef = document.getElementById('report-table').getElementsByTagName('tfoot')[0];
        var totalsRow = tfootRef.insertRow(tfootRef.rows.length);
        totalsRow.insertCell(0).appendChild(document.createTextNode("Total"));
        totalsRow.insertCell(1).appendChild(document.createTextNode(backgroundPage.getTimeString(total, true)));
    });
}

function saveDomainsFromTextarea(callback) {
    var domains = sanitizeDomains(document.getElementById("domains").value.split("\n"));

    chrome.storage.local.set({'domains': domains}, callback);
}

function toggleNotificationsCheckbox() {
    chrome.extension.getBackgroundPage().getNotificationSetting(function(items) {
        document.getElementById("notifications").checked = items['config.notifications'];
    });
}
function setNotificationsFromCheckbox(callback) {
    var showNotifications = document.getElementById("notifications").checked;

    chrome.storage.local.set({'config.notifications': showNotifications}, callback);
}

function sanitizeDomains(domains) {
    var backgroundPage = chrome.extension.getBackgroundPage();
    for(var x in domains) {
        domains[x] = backgroundPage.getDomainFromUrl(domains[x]);
    }

    return uniq(domains);
}

function uniq(a) {
    return a.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    })
}

function getStatus(url) {
    var backgroundPage = chrome.extension.getBackgroundPage();

    backgroundPage.getDomains(function(items) {
        var domains = items.domains;
        if(backgroundPage.isSlackDomain(url, domains)) {
            backgroundPage.getTimer(function(timer, isActive) {
                if(isActive) {
                    renderStatus("Slacking for " + backgroundPage.getTimeString(Date.now() - timer.timestamp, true) + " on " + backgroundPage.getDomainFromUrl(url));
                }
                else {
                    renderStatus("You're slacking as we speak");
                }
            });
        }
        else {
            renderStatus("Sweet, you're not slacking");
        }
    });
}

document.addEventListener('DOMContentLoaded', function() {
    populateDomainsTextarea();
    toggleNotificationsCheckbox();
    showReport();

    document.getElementById("submit-configuration").addEventListener("click", function(){
        setNotificationsFromCheckbox(function() {});
        saveDomainsFromTextarea(function() {
            // Notify that we saved.
            var backgroundPage = chrome.extension.getBackgroundPage();
            backgroundPage.createNotification(null, "Domains saved", "The list of slack domains was updated.", backgroundPage.config.icons.save);
            window.close();
        });
    });

  getCurrentTabUrl(function(url) {
    // Put the image URL in Google search.
    getStatus(url);
  });
});

