var activeTab = {"url": ""};

var config = {
    "icons": {
        "default": "smile_grin_43.png",
        "slack": "smile_sad_43.png",
        "save": "floppy_disk_48.png",
        "notification": "thumbs_up_48.png"
    }
};

var titles = [
    'Nice one!',
    'Slackin\' again \'eh?',
    'Congrats. You\ did it again.',
    'Loser. Get back to work.',
    'Idiot. Get shit done',
    'Did that make you feel good?',
    'Really? Again?',
    'Do you feel like you deserve it? You don\'t.',
    'So proud of you.',
    'U so productive. Me so proud.',
    'How\'s that report coming along?',
    'Work got too tough? Tsk.',
    'Give yourself a round of applause.'
];

function getRandomTitle() {
    return titles[getRandomInt(0, titles.length-1)];
}

function isSlackDomain(url, domains) {
    for(var x in domains) {

        var re = new RegExp("^http(s?)\:\/\/(www\.)?" + domains[x] + "(.+)?", "i");

        if(re.test(url)) {
            return true;
        }
    }

    return false;
}

function getDomains(callback) {
    chrome.storage.local.get('domains', callback);
}

function initSlackOMeter(url) {
    var domain = getDomainFromUrl(url);

    getDomains(function(items) {
        if(isSlackDomain(url, items.domains)) {
            getTimer(function(timer, isActive) {
                if(timer.domain === domain) {
                    console.log("Continuing on same domain " + domain);
                    return;
                }

                if(isActive) {
                    console.log("Timer already active for domain " + timer.domain + " while currently on " + domain + ". WTF?");
                    return;
                }

                startTimer(domain);
            });
        }
    });
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function getChromeMajorVersion() {
    return /Chrome\/([0-9]+)/.exec(navigator.userAgent)[1];
}

function getNotificationSetting(callback) {
    chrome.storage.local.get('config.notifications', callback);
}

function createNotification(id, title, message, icon) {
    getNotificationSetting(function(items) {
        if(!items['config.notifications']) {
            return;
        }

        icon = (typeof icon === 'undefined' ? config.icons.notification : icon);

        // Notification ID is required before Chrome 42
        if(id === null && getChromeMajorVersion() < 42) {
            id = getRandomInt(100000, 999999);
        }

        chrome.notifications.create(id, {
            "type": "basic",
            "iconUrl": icon,
            "title": title,
            "message": message
        }, function(notificationID) {});
    });
}

function getDomainFromUrl(url) {
    return url.replace(/^(http(s?)\:\/\/)?(www\.)?([^\/]+)(\/)?(.+)?$/i, "$4");
}

function changeIcon(path) {
    chrome.browserAction.setIcon({
        "path": path
    });
}

function setSlackingIcon() {
    changeIcon(config.icons.slack);
}

function setDefaultIcon() {
    changeIcon(config.icons.default);
}

function startTimer(domain) {
    chrome.storage.local.set({"timer": {"domain": domain, "timestamp": Date.now()}}, function() {console.log("Started timer for " + domain);});

    setSlackingIcon();
}
function stopTimer(domain, callback) {
    chrome.storage.local.get("timer", function(items) {
        if(typeof items.timer === 'undefined') {
            //createNotification(null, "Whoops", "Could not register slack time!");
            return;
        }

        if(items.timer.domain === domain) {
            addLog(domain, items.timer.timestamp, Date.now());
        }
        else {
            createNotification(null, "Error!", "Domain " + domain + " doesn't match " + items.timer.domain);
        }

        chrome.storage.local.remove("timer", callback);

        setDefaultIcon();
    });
}
function getTimer(callback) {
    chrome.storage.local.get("timer", function(items) {
        if(typeof items.timer === 'undefined') {
            callback(undefined, false);
        }
        else {
            callback(items.timer, true);
        }
    })
}

function getTimeString(timeSpentMillis, short) {
    short = (typeof short !== 'undefined');
    var seconds = timeSpentMillis / 1000;

    if(seconds < 60) {
        if(seconds < 2) {
            return "1" + (short ? 's' : ' second');
        }
        return Math.ceil(seconds) + (short ? 's' : ' seconds');
    }

    var minutes = Math.floor(seconds / 60);

    if(minutes < 60) {
        if(minutes < 2) {
            return "1" + (short ? 'm' : ' minute') + ' ' + Math.floor(seconds - (minutes * 60)) + (short ? 's' : ' seconds');
        }
        return minutes + (short ? 'm' : ' minutes') + ' ' + Math.floor(seconds - (minutes * 60)) + (short ? 's' : ' seconds');
    }

    var hours = minutes / 60;

    if(hours < 24) {
        if (hours < 2) {
            return "1" + (short ? 'h' : ' hour');
        }
        return Math.ceil(hours) + (short ? 'h' : ' hours');
    }
}

function addLog(domain, timeStart, timeEnd) {
    // if slacked for less than 10s, don't show notification
    //if(timeEnd - timeStart < 10000) {
    //    return;
    //}

    var today = getTodayString();
    var key = "log.days." + today;
    var timeSpent = timeEnd - timeStart;


    chrome.storage.local.get(key, function(items) {
        if(typeof items[key] === 'undefined') {
            items[key] = {};
        }

        if(typeof items[key][domain] === 'undefined') {
            items[key][domain] = 0;
        }

        if(domain == "" || typeof domain === 'undefined') {
            return;
        }

        items[key][domain] += timeSpent;

        var obj = {};
        obj[key] = items[key];

        chrome.storage.local.set(obj, function() {
                createNotification(null, getRandomTitle(), "You slacked for " + getTimeString(timeEnd - timeStart) + " on " + domain);
            }
        );
    });
}

function getTodayString() {
    return new Date().toJSON().slice(0,10);
}

//noinspection JSUnusedGlobalSymbols
function getDailyReport(callback) {
    var today = getTodayString();

    var key = "log.days."+today;

    chrome.storage.local.get(key, callback);
}

function getLastFocusedWindow(callback) {
    chrome.windows.getLastFocused({"populate": true}, callback);
}

function backgroundTimerManagement() {
    getTimer(function(timer, isTimerActive) {
        getLastFocusedWindow(function(window) {
            if(isTimerActive && !window.focused) {
                // stop the timer!
                stopTimer(timer.domain);
            }
            else if(!isTimerActive && window.focused) {
                // Check whether the currently active tab is a 'slack domain'.
                chrome.tabs.query({
                    "active": true,
                    "windowId": window.id
                }, function(tabs) {
                    if(typeof tabs[0] === 'undefined') {
                        return;
                    }

                    getDomains(function(items) {
                        if(isSlackDomain(tabs[0].url, items.domains)) {
                            startTimer(getDomainFromUrl(tabs[0].url));
                        }
                    });
                });
            }
        });
    });
}

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if(typeof tab === 'undefined') {
        return;
    }

    if(changeInfo.status !== "complete") {
        return;
    }

    initSlackOMeter(tab.url);
});

chrome.tabs.onActivated.addListener(function(activeInfo) {
    var prevActiveTab = activeTab;

    chrome.tabs.get(activeInfo.tabId, function(tab) {
        activeTab = tab;

        var domain = getDomainFromUrl(tab.url);

        getDomains(function(items) {
            if(isSlackDomain(prevActiveTab.url, items.domains)) {
                var prevDomain = getDomainFromUrl(prevActiveTab.url);

                stopTimer(prevDomain, function() {
                    if(isSlackDomain(tab.url, items.domains)) {
                        startTimer(domain);
                    }
                });
            }
            else {
                if(isSlackDomain(tab.url, items.domains)) {
                    startTimer(domain);
                }
            }
        });
   });
});

window.setInterval(backgroundTimerManagement, 5000);